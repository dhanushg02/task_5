#!/bin/bash

# Compile Java program
javac SimpleJavaApplication.java

# Check if compilation was successful
if [ $? -eq 0 ]; then
    # Run the compiled program
    java SimpleJavaApplication
else
    echo "Compilation failed. Cannot run the program."
fi
